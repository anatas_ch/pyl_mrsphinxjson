==============================
mrsphinxjson - JSON for Sphinx
==============================

How to use it
=============

.. code-block:: ReST

   .. json :: <filename>

The ``filename`` is relative to the source directory.

Test JSON
=========

.. json:: demo1.json

.. json:: ../tests/test01.json


ToDo-List
=========

.. todolist::

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
